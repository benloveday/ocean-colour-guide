.. _front_page:

Welcome to the fundamentals of ocean community resource
=======================================================

.. image:: ../../img/FoOC_banner.png
  :width: 100%
  :alt: banner image

Summary
----------------
"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

Using these materials
----------------
This documentation is distrbuted under the `Creative Commons CC-BY-SA-4.0 <https://creativecommons.org/licenses/by-sa/4.0/deed.en>`_ license. You are free to **share** and **adapt** the materials for any purpose. Please see the license text for a full description of the terms.

If you wish to contribute to this guide, make suggestions for adaptations, or suggest a correction for any inaccuracy, please see the :ref:`contributor guidelines<_contributions>` section.

Contents
--------
.. toctree::
   :maxdepth: 2

   01_contributors
   02_introduction
   03_rationale
   04_mission_timeline
   05_principles
   06_measurements
   07_corrections
   08_variables
   09_applications
   10_resources
   11_contributions

------------

