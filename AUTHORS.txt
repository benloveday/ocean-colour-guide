# Copyright holder
copyright: CC BY-SA 4.0 (2025)
license: LICENSE.txt

# List of development authors
Ben Loveday (Innoflair UG / EUMETSAT)

# Support
For all queries on this software package, please raise an issue at https://gitlab.com/benloveday/fundamentals-of-ocean-colour
